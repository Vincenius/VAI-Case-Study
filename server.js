const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const routes = require('./app/routes');
const helpers = require('./app/helpers');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/', routes);

helpers.initDatabase();

//_____Server auf Port starten_____//
http.listen(3000, function () {
    console.log('listening on *:3000');
});