const mongoose = require('mongoose');
const initWords = ['do', 'got', 'is', 'have', 'and', 'although', 'or', 'that', 'when', 'while', 'a', 'either',
                    'more', 'much', 'neither', 'my', 'that', 'the', 'as', 'no', 'nor', 'not', 'at', 'between', 
                    'in', 'of', 'without', 'i', 'you', 'he', 'she', 'it', 'we', 'they', 'anybody', 'one'];
var Word = require('./models/word');
var db;

// helper function
String.prototype.replaceAll = function (search, replacement) {
    let target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

module.exports = {
    initDatabase: async function() {
        // connect to database
        mongoose.connect('mongodb://localhost/nonlexicalwords', { useNewUrlParser: true });
        db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log('database connected');
        });

        // push words to database if DB is empty
        let wordsInDb = await Word.find();
        if (wordsInDb.length === 0) {
            console.log('saving words to database...');
            let savePromises = [];
            for (let word of initWords) {
                let newWord = new Word({ text: word });
                savePromises.push(newWord.save());
            }

            Promise.all(savePromises).then(function() {
                console.log('all words saved!');
            });
        }
    },
    isValidLength: function(text) {
        let charCount = text.length;
        let wordCount = text.split(/\s+/).length;

        return (charCount <= 1000 && wordCount <= 100);
    },
    calculateDensity: function(text, nonLexicalWords) {
        let nonLexicalWordCount = 0;
        text = text.replaceAll(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,""); // remove all punctuation
        let textWordArray = text.split(/\s+/);

        // count non lexical words in text
        for (let word of textWordArray) {
            if (nonLexicalWords.includes(word)) {
                nonLexicalWordCount++;
            }
        }

        // calculate density
        let density = parseFloat(((textWordArray.length - nonLexicalWordCount) / textWordArray.length).toFixed(2));

        return density;
    },
    getDensity: async function(text, mode) {
        // get words from database
        let wordsInDb = await Word.find().select({ 'text': 1, '_id': 0});
        let nonLexicalWords = wordsInDb.map(wordObj => wordObj['text']);
        let data;

        if (mode === 'verbose') {
            // split into sentence
            let sentencesArray = text.split('.');
            let overallDensity = 0;
            let sentenceDensity = [];
            
            for (let sentence of sentencesArray) {
                // get density for each sentence
                let density = this.calculateDensity(sentence, nonLexicalWords);
                sentenceDensity.push(density);
                overallDensity += density;
            }

            overallDensity = parseFloat((overallDensity / sentencesArray.length).toFixed(2));
            
            data = { 
                'sentence_ld': sentenceDensity,
                'overall_ld': overallDensity 
            };
        } else {
            // get density for whole text
            let density = this.calculateDensity(text, nonLexicalWords);
            data = { 'overall_ld': density };
        }

        return data;
    }
};