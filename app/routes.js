const express = require('express');
const router = express.Router();
const helpers = require('./helpers');

router.post('/complexity', async function (req, res, next) {
    let text = req.body.text.toLowerCase();
    let mode = req.query.mode.toLowerCase();
    
    if (helpers.isValidLength(text)) {
        let data = await helpers.getDensity(text, mode);

        res.send({ data }); // shorthand for {data: data}
    } else {
        res.status(400);
        res.send({ error: 'Only texts with up to 100 words or up to 1000 characters are valid.' });
    }
});

module.exports = router;