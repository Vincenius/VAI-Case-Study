const helpers = require('../app/helpers');
const assert = require('assert');

describe('Helper Functions', function() {
    describe('isValidLength(text) function', function() {
        it('should return true if text < 1000 chars and < 100 words', async function(){
            let text = 'This is a demo text. It has 53 chars and 2 sentences.'
            assert.equal(helpers.isValidLength(text), true);
        });
        it('should return false if text > 1000 chars', async function(){
            let text = 'This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. This is a demo text. It has over 1000 chars. '
            assert.equal(helpers.isValidLength(text), false);
        });
        it('should return false if text > 100 words', async function(){
            let text = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem'
            assert.equal(helpers.isValidLength(text), false);
        });
    });

    describe('calculateDensity(text, nonLexicalWords) function', function() {
        it('should return 0 if all words of "text" are in "nonLexicalWords"', async function(){
            let text = 'this is a test.';
            let nonLexicalWords = ['this', 'a', 'is', 'the', 'test'];
            assert.equal(helpers.calculateDensity(text, nonLexicalWords), 0);
        });

        it('should return 1 if no words of "text" are in "nonLexicalWords"', async function(){
            let text = 'this is a test.';
            let nonLexicalWords = ['that', 'an', 'it'];
            assert.equal(helpers.calculateDensity(text, nonLexicalWords), 1);
        });

        it('should return 0.5 if half words of "text" are in "nonLexicalWords"', async function(){
            let text = 'this is a test.';
            let nonLexicalWords = ['this', 'an', 'test'];
            assert.equal(helpers.calculateDensity(text, nonLexicalWords), 0.5);
        });
    });

    describe('getDensity(text, mode) function', function() {
        before(async function() {
            await helpers.initDatabase();
        })
        it('should only return overall_ld when no mode provided', async function(){
            let text = 'this is a test.';
            let result = await helpers.getDensity(text);
            
            assert.equal(result.sentence_ld, undefined);
            assert.equal(result.overall_ld, 0.5);
        });
        it('should return overall_ld and sentence_ld when mode is verbose', async function(){
            let text = 'this is a test. with a second sentence';
            let result = await helpers.getDensity(text, 'verbose');
            
            assert.equal(result.sentence_ld[0], 0.5);
            assert.equal(result.sentence_ld[1], 0.8);
            assert.equal(result.overall_ld, 0.65);
        });
    });
})